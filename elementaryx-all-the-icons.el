;;; ElementaryX: Elementary Emacs configuration coupled with Guix
;;;
;;; emacs-all-the-icons setup for:
;;; - emacs-all-the-icons
;;; - emacs-all-the-icons-completion
;;; - emacs-all-the-icons-dired
;;; - emacs-all-the-icons-ibuffer
;;; - emacs-spaceline-all-the-icons
;;; - emacs-treemacs-all-the-icons

;;; Usage: This can be used as a standalone package, independently of
;;; the rest of the elementaryx suite, e.g.:
;;;   `guix shell emacs emacs-elementaryx-all-the-icons`

(use-package all-the-icons
  :if (display-graphic-p))

(use-package all-the-icons-dired
  :after all-the-icons
  :if (display-graphic-p)
  :hook
  (dired-mode . all-the-icons-dired-mode)
  :config (setq all-the-icons-dired-monochrome nil)) ;; https://emacs.stackexchange.com/questions/71269/all-the-icons-are-all-white-in-dired

(use-package all-the-icons-completion
  :after (marginalia all-the-icons)
  :if (display-graphic-p)
  :hook (marginalia-mode . all-the-icons-completion-marginalia-setup) ;; TODO check relationship with emacs-kind-icon?
  :init
  (all-the-icons-completion-mode))

(use-package all-the-icons-ibuffer
  :after all-the-icons
  :if (display-graphic-p)
  :hook (ibuffer-mode . all-the-icons-ibuffer-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Mode line: spaceline-all-the-icons
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Mode-Line.html

(use-package spaceline
  :config
  (spaceline-spacemacs-theme))

;; (use-package spaceline-all-the-icons
;;   :after all-the-icons
;;   :if (display-graphic-p)
;;   :config
;;   ;; Load spaceline-all-the-icons
;;   (spaceline-all-the-icons-theme)
;;   ;; Optional dependencies
;;   ;; (spaceline-all-the-icons--setup-anzu)            ;; Enable anzu searching
;;   ;; (spaceline-all-the-icons--setup-package-updates) ;; Enable package update indicator
;;   (spaceline-all-the-icons--setup-git-ahead) ;; Enable # of commits ahead of upstream in git
;;   ;; (spaceline-all-the-icons--setup-paradox)         ;; Enable Paradox mode line
;;   ;; (spaceline-all-the-icons--setup-neotree)         ;; Enable Neotree mode line
;;   ;;
;;   ;; Elementaryx relies on built-in project.el rather than projectile:
;;   (spaceline-toggle-all-the-icons-projectile-off)
;;   ;; Support emacs-envrc (direnv)
;;   (defface elementaryx-spaceline-envrc-face
;;     '((t (:foreground "plum1" :distant-foreground "DarkMagenta")))
;;     "Face for highlighting the direnv environment, handled with emacs-envrc."
;;     :group 'spaceline)
;;   (defun elementaryx-spaceline-envrc-status ()
;;     "Get the status of envrc."
;;     (if (and (boundp 'envrc--status) (eq envrc--status 'on))
;; 	(propertize "\u2622[direnv]" 'face 'elementaryx-spaceline-envrc-face)
;;       ""))
;;   (spaceline-define-segment elementaryx-spaceline-envrc-segment
;; 			    "The current envrc environment. Works with direnv."
;; 			    (elementaryx-spaceline-envrc-status))
;;   (spaceline-all-the-icons-theme 'elementaryx-spaceline-envrc-segment))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Treemacs: treemacs-all-the-icons
;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; treemacs theme using all-the-icons
(use-package treemacs-all-the-icons
  :after treemacs
  :config (treemacs-load-theme "all-the-icons"))

;; BUG:
;; funcall-interactively: Wrong type argument: hash-table-p, nil [2 times]
;; https://github.com/Alexander-Miller/treemacs/issues/166

;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ;;;
;; ;;; Theme: doom-themes: doom-one
;; ;;;
;; ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;; Note: not dependent on all-the-icons
;; ;; Waiting "nerd icons" integration withing guix for handling that separately
;; (use-package doom-themes
;;   :config
;;   ;; Global settings (defaults)
;;   (setq doom-themes-enable-bold t ; if nil, bold is universally disabled
;;         doom-themes-enable-italic t) ; if nil, italics is universally disabled
;;   (load-theme 'doom-one t)

;;   ;; Enable flashing mode-line on errors
;;   (doom-themes-visual-bell-config)
;;   ;; Enable custom neotree theme (all-the-icons must be installed!)
;;   ;; (doom-themes-neotree-config)
;;   ;; or for treemacs users
;;   (setq doom-themes-treemacs-theme "doom-atom") ; use "doom-colors" for less minimal icon theme
;;   (doom-themes-treemacs-config)
;;   ;; Corrects (and improves) org-mode's native fontification.
;;   (doom-themes-org-config)
;;   )

;; ;; (setq fancy-splash-image (file-name-concat "/tmp/" "elementaryx.png"))
;; ;; (setq fancy-splash-image "/tmp/ElementaryX-fs8.png")

(provide 'elementaryx-all-the-icons)
